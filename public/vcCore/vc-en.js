
/**
 * 英文包
 */
(function(window){
    window.lang={
        "systemName":"CommunityManagement",
        "systemSimpleName":"SR",
        "subSystemName": "CommunityManagement",
        "companyTeam":"sky Round data team",
        "welcome":"welcome community management",
        "signOut":"sign out",
        "signIn":"sign in",
        "register":"register",
        "moreCommunity":"more community",
        "moreMsg":"more message",
        "title":"CommunityManagement",
        "noAccount":"no account?",
        "areyouhasaccount":"are you has account?",
        "我的小区":"my community"
    }
})(window)